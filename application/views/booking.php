<div class="Select Seats">
    <h4>Select Seats</h4>
    <form method="post" action="reservation.php">
    <div class="row">
        <div class="large-4 columns">
            <input name="route_id" type="hidden" value="<?php echo $route_id;?>" />
            <table>
                <tr><td>Origin:</td><td><?php echo $origin;?></td></tr>
                <tr><td>Destination:</td><td><?php echo $destination;?></td></tr>
                <tr><td>Departure Time:</td><td><?php echo $depart_time;?></td></tr>
                <tr><td>Departure Date:</td><td><?php echo $depart_date;?>
                <input name="depart_date" type="hidden" value="<?php echo $depart_date;?>" /></td></tr>
                <tr><td>Est. Travel Hrs:</td><td><?php echo $travel_hours;?></td></tr>
                <tr><td>Est. Arrival Time:</td><td><?php echo $arrival_time;?></td></tr>
                <tr><td>Bus No:</td><td><?php echo $bus_no;?></td></tr>
                <tr><td>Seat Numbers:</td><td><input id="seatNos" name="seat_nos" type="text" required readonly /></td></tr>
                <tr><td>Total Seats:</td><td><input id="totalSeats" name="total_seats" type="text" value="0" readonly /></td></tr>
                <tr><td>Total Price:</td><td><input id="totalPrice" name="total_price" type="text" value="Php 0.00" readonly /></td></tr>
            </table>
        </div>
        <div class="large-8 columns">
            <h5>Seats</h5>
            <p><img src="img/available_seat_img.gif"> - Available   
            <img src="img/selected_seat_img.gif"> - Selected   
            <img src="img/booked_seat_img.gif"> - Booked</p>
            <table>
                <?php
                    if ($num_seats != 0) {
                        $length = ($num_seats - 1)/ 4;
                        $ln;
                        for ($i = 1; $i <= 4; $i++) {
                            if ($i == 3) {
                                echo "<tr>";
                                for ($i1 = 1; $i1 <= $length; $i1++) {
                                    echo "<td>";
                                    if ($i1 == $length) {
                                        echo "<button class='seat' id='seat$ln' name='seatNo$ln' type='button' value='$ln'";
                                        if (in_array($ln, $rseats)) {
                                            echo"><img src='img/booked_seat_img.gif'>";
                                        } else {
                                            echo" onclick='seatClicked($ln)'><img src='img/available_seat_img.gif'>";
                                        }
                                        echo "<br>$ln</button>";
                                    }
                                    echo "</td>";
                                }
                                echo "</tr>";		
                            }
                            echo "<tr>";
                            $s = $i;
                            for ($i1 = 1; $i1 <= $length; $i1++) {
                                echo "<td><button class='seat' id='seat$s' name='seatNo$s' type='button' value='$s'";
                                if (in_array($s, $rseats)) {
                                    echo"><img src='img/booked_seat_img.gif'>";
                                } else {
                                    echo" onclick='seatClicked($s)'><img src='img/available_seat_img.gif'>";
                                }
                                echo "<br>$s</button></td>";	
                                if ($i == 2 ) {
                                    $ln = $s + 1;	
                                }
                                $s += 4;
                                if ($i > 2 && $i1 == ($length - 1)) {
                                    $s++;
                                }
                            }
                            echo "</tr>";
                        }
                    }
                ?>
            </table>
            <button class="button" name="submit" type="submit">Submit</button>
        </div>
    </div>
    </form>
</div>
<script>
var seats = [];
var numSeats = 0;
var price = <?php echo $price;?>;
function seatClicked(seatNo) {
	var i = seats.indexOf(seatNo);
	if (i == -1) {
		seats.push(seatNo);
		numSeats++;
		$("#seat" + seatNo + " img").prop("src","img/selected_seat_img.gif");
	} else {
		seats.splice(i, 1);	
		numSeats--;
		$("#seat" + seatNo + " img").prop("src","img/available_seat_img.gif");
	}
	seats.sort();
	var str = "";
	for (var i = 0; i < seats.length; i++) {
		str += seats[i];
		if (i < seats.length -1) {
			str += ", ";	
		}
	}
	var totalPrice = "Php " + Number(price * numSeats).toFixed(2);
	$("#seatNos").val(str);
	$("#totalSeats").val(numSeats);
	$("#totalPrice").val(totalPrice);
}
</script>