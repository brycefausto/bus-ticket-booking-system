<div class="user-login">
    <div class="grid-x">
        <div class="cell large-4">
            <h4>Login</h4>
            <?php show_callout($error); ?>
            <form method="post">
                <input name="login_user" type="text" placeholder="Email/Username" autcomplete="username" />
                <input name="login_passwd" type="password" placeholder="Password" autocomplete="new-password" />
                <button class="button expanded" name="login" type="submit">Login</button>
            </form>
            <div class="text-center"><a data-open="retrieveModal">Forgot Password?</a></div>
            <div class="reveal tiny" id="retrieveModal" data-reveal>
                <form method="post">
                <input name="email" type="email" placeholder="Your email"/><br />
                <button class="button" name="retrieve" type="submit">Retrieve Password</button>
                </form>
            </div>
            <div style="margin-top:40px">
                <form method="post">
                    <button class="button expanded" name="register" type="submit">Register</button>
                </form>
            </div>
        </div>
    </div>
</div>