<div class="user-register">
	<h4>Register</h4>
    <div class="grid-x">
        <div class="large-4 cell">
            <form method="post" data-abide novalidate>
                <div>
                    <?php show_callout($error); ?>
                </div>
                <div>
                    <input name="username" type="text" placeholder="Username" aria-errormessage="usernameError" autocomplete="username" required />
                    <span class="form-error" id="usernameError">The username is required.</span>
                </div>
                <div>
                    <input id="password" name="password" type="password" placeholder="Password" autocomplete="new-password" aria-errormessage="passwordError" pattern=".{5,16}" 
                    aria-describedby="help-password" required />
                    <span class="form-error" id="passwordError">The password is required.</span>
                    <p class="help-text" id="help-password">The password must have 5-16 characters.</p>
                </div>
                <div>
                    <input name="passconf" type="password" placeholder="Confirm Password" autocomplete="new-password" data-equalto="password" required />
                    <span class="form-error">The passwords must match.</span>
                </div>
                <div>
                    <input name="full_name" type="text" placeholder="Full Name"  />
                    <span class="form-error">The full name is required.</span>
                </div>
                <div>
                    <input name="contact" type="text" placeholder="Contact" pattern="[0-9]{11}"/>
                    <span class="form-error">The contact is invalid.</span>
                </div>
                <div>
                    <input name="email" type="text" placeholder="Email" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" />
                    <span class="form-error">The email is invalid.</span>
                </div>
                <div>
                    <input name="address" type="text" placeholder="Address"  />
                    <span class="form-error">The address is required.</span>
                </div>
                <button class="button expanded" name="register" type="submit">Register</button>
            </form>
        </div>
    </div>
</div>
