<div class="header">
	<div class="logo">
    	<img src="<?php echo assets_url('img/partas_banner.jpg'); ?>">
    </div>
	<div class="nav">
    	<div class="top-bar" data-topbar role="navigation">
            <div class="top-bar-left">
                <ul class="dropdown menu" data-dropdown-menu>
                    <li><a href="<?php echo base_url(); ?>"><i class="fi-home"></i> Home</a></li>
                    <li><a href="routes"><i class="fi-compass"></i> Routes</a></li>
                    <li><a href="location"><i class="fi-map"></i> Location</a></li>
                    <li><a href="contact"><i class="fi-telephone"></i> Contact Us</a></li>
                </ul>
            </div>
            <div class="top-bar-right">
            <?php 
            if (isset($_GET['logout'])) {
                session_unset();
                header("location: index.php");	
            }
            if (isset($_SESSION['user_id'])): ?>
            <ul class="menu menu-user">
                <li><a href="reservation"><i class="fi-book"></i> My Reservation</a></li>
                <li><a href="profile"><i class="fi-torso"></i> Profile</a></li>
                <li><a href="<?php echo base_url(); ?>?logout"><i class="fi-power"></i> Log Out</a></li>
            </ul>
            <?php else: ?>
            <ul class="menu menu-guest">
                <li><a href="admin/home"><i class="fi-torso"></i> Admin</a></li>
                <li><a href="register"><i class="fi-address-book"></i> Register</a></li>
                <li><a href="login"><i class="fi-torso"></i> Login</a></li>
            </ul>
            <?php endif; ?></div>
    	</div>
	</div>
</div>