<div class="routes">
    <h4>Routes</h4>
    <form method="get" action="booking">
    <div class="row">
        <div class="large-4 column">
            <input class="date-input" name="depart_date" id="dp" type="text" readonly="readonly" style="cursor:pointer" 
    placeholder="Select Date" required value="<?php echo date("m-d-Y", strtotime("+3 days")); ?>"/>
            <span class="form-error">Please select a date.</span>
        </div>
    </div>
    <p>Est. - Estimated<p>
    <table class="route-table">
        <thead>
            <th>Origin</th>
            <th>Destination</th>
            <th>Departure</th>
            <th>Est. Travel Hrs</th>
            <th>Est. Arrival Time</th>
            <th>Price</th>
            <th></th>
        </thead>
        <tbody>
            <?php
            foreach ($routes as $route) {
                extract($route);
                $price = "Php ".number_format($price, 2, '.', '');
                echo "<tr><td>$origin</td>
                <td>$destination</td>
                <td>$depart_time</td>
                <td>$travel_hours</td>
                <td>$arrival_time</td>
                <td>$price</td>
                <td><button class='button table-button' name='route_id' type='submit' value='$id'>Book Ticket</button></td></tr>";	
            }
            ?>
        </tbody>
    </table>
    </form>
</div>
