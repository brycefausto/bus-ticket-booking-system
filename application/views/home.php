<div class="ticket-booking">
    <div class="grid-x">
        <div class="cell large-4">
            <h4>Ticket Booking</h4>
            <form method="get" action="search.php" data-abide>
                    <?php
                    $options = array_merge(['Select Origin'], $locations);
                    echo form_dropdown('origin', $options, 'required');
                    $options = array_merge(['Select Destination'], $locations);
                    echo form_dropdown('destination', $options, 'required');
                    ?>
                    <input class="date-input" name="date" id="dp" type="text" readonly="readonly" style="cursor:pointer" placeholder="Select Date" required value="<?php echo date("m-d-Y", strtotime("+3 days")); ?>" />
                    <span class="form-error">Please select a date.</span>
                    <button class="button expanded" type="submit"><i class="small-font fi-magnifying-glass"></i> Search</button>
            </form>
        </div>
    </div>
</div>