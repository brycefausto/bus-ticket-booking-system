<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_model extends CI_Model {

    protected $table = 'customers';

	public function get_user($id)
	{
        return $this->db->select('username, fullName')
            ->get_where($this->table, ['id' => $id])->row();
    }

    public function login($username, $password) {
        return $this->db->select('id, full_name')->from($this->table)
            ->group_start()
                ->where('username', $username)
                ->or_where('email', $username)
            ->group_end()
            ->where('password', $password)
            ->get()->first_row();
    }
    
    public function create($inputs) {
        return $this->db->insert($this->table, $inputs);
    }
}
