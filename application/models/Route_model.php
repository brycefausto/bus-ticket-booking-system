<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Route_model extends CI_Model {

    protected $table = 'routes';

	public function get_list()
	{
        return $this->db->get($this->table)->result_array();
    }

    public function get($id) {
        return $this->db->get_where($this->table, ['id' => $id], 1)->row_array(0);
    }

}
