<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reserve_model extends CI_Model {

    protected $table = 'reserves';

	public function get_seat_nos($origin, $destination, $depart_time, $depart_date)
	{
        $where = array_merge(compact('origin', 'destination', 'depart_time', 'depart_date'), ['status !=' => 'Cancelled']);
        $result = $this->db->select('seat_nos')->get_where($this->table, $where)->result();
        $rseats = [];
        foreach ($result as $row) {
            $rseats = array_merge($rseats, explode(', ', $row->seat_nos));
        }
        return $rseats;
    }

    public function get($id) {
        return $this->db->get_where($this->table, ['id' => $id], 1)->row_array(0);
    }

}
