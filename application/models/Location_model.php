<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Location_model extends CI_Model {

    protected $table = 'locations';

	public function get_list()
	{
        $query = $this->db->get($this->table);
        $locations = [];
        foreach ($query->result() as $row) {
            $locations[] = $row->name;
        }
        return $locations;
	}
}
