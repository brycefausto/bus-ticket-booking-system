<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('Location_model');
	}

	public function index()
	{
		$data['locations'] = $this->Location_model->get_list();
		$this->load->template('home', $data);
	}
}
