<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Routes extends CI_Controller {

    function __construct()
	{
		parent::__construct();
        $this->load->model('route_model');
        $this->load->model('reserve_model');
	}

	public function index()
	{
        $data['routes'] = $this->route_model->get_list();
		$this->load->template('routes', $data);
    }
    
    public function show_booking() {
        $input = $this->input;
        $route_id = $input->get('route_id') || 0;
        $depart_date = $input->get('depart_date') || '';
        if ($this->session->has_userdata('user_id')) {
            redirect("/login.php?route_id=$route_id&depart_date=$depart_date");
        }
        $route = $this->route_model->get($route_id);
        $origin = '';
        $destination = '';
        $depart_time = '';
        $arrival_time = '';
        $travel_hours = '';
        $bus_no = '';
        $num_seats = '';
        $price = '';
        if ($route) {
            log_message('debug', print_r($route, true));
            extract($route);
        } else {
            log_message('debug', 'No route found');
            log_message('debug', 'Last Query: '.$this->db->last_query());
        }
        $rseats = $this->reserve_model->get_seat_nos($origin, $destination, $depart_time, $depart_date);
        $data = compact('route_id','depart_date','origin', 'destination', 'depart_time', 'arrival_time', 'travel_hours', 'bus_no', 'num_seats', 'price', 'rseats');
        $this->load->template('booking', $data);
    }
}
