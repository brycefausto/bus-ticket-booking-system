<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('user_model');
		$this->load->library('form_validation');
	}

	public function login()
	{
		if ($this->session->has_userdata('login')) {
			redirect('/');
		}
		$data = [];
		$input = $this->input;
		if ($input->method() === 'post') {
			$validator = $this->form_validation;
			$validator->set_rules('login_user', 'Username', 'required');
			$validator->set_rules('login_passwd', 'Password', 'required');
			if ($validator->run() == FALSE) {
				$data['error'] = $validator->error_string();
			} else {
				$username = $input->post('login_user', TRUE);
				$password = $input->post('login_passwd', TRUE);
				$user = $this->user_model->login($username, $password);
				if ($user) {
					$this->session->set_userdata([
						'login' => TRUE,
						'user_id' => $user->id,
						'full_name' => $user->full_name
					]);
					redirect('/');
				} else {
					$data['error'] = 'The username or password is invalid';
				}
			}
		}
		$this->load->template('login', $data);
	}

	public function register() {
		$validator = $this->form_validation;
		$validator->set_rules('username', 'Username', 'required|is_unique[customers.username]',
			['is_unique' => 'The %s already exists.']);
		$validator->set_rules('password', 'Password', 'required');
		$validator->set_rules('passconf', 'Confirm Password', 'required|matches[password]');
        $validator->set_rules('full_name', 'Full Name', 'required');
        $validator->set_rules('email', 'Email', 'required|valid_email|is_unique[customers.email]');
        if ($validator->run() == FALSE) {
            $data['error'] = $validator->error_array();
		} else {
			$fields = ['username', 'password', 'full_name', 'email', 'contact', 'address'];
			$inputs = $this->input->post($fields, TRUE);
			if ($this->user_model->create($inputs)) {
				redirect('/');
			} else {
				$data['error'] = 'Unable to create user';
			}
		}
		$this->load->template('register', $data);
	}

	public function logout() {
		session_unset();
		redirect('/');
	}

}
