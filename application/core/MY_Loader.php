<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Loader extends CI_Loader {

	public function template($view, $vars = array())
	{
        $data['css_paths'] = [
            'foundation.min.css',
            'foundation-datepicker.min.css',
            'foundation-icons.css',
            'app.css'
        ];
        $data['js_paths'] = [
            'jquery.js',
            'what-input.js',
            'foundation.min.js',
            'foundation-datepicker.min.js',
            'app.js'
        ];
        $content = $this->view($view, $vars, TRUE);
        $js = "";
        preg_match_all('#<script(.*?)</script>#is', $content, $matches);
        foreach ($matches[0] as $value) {
            $js .= $value."\n";
        }
        $content = preg_replace('#<script(.*?)</script>#is', '', $content); 
        $data['content'] = $content;
        $data['javascripts'] = $js;
        $this->view('templates/index', $data);
	}
}
