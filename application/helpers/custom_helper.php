<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if ( ! function_exists('assets_url'))
{
    function assets_url($uri = '')
    {
        return base_url('assets/').$uri;
    }
}

if ( ! function_exists('css_path'))
{
    function css_path($uri = '')
    {
        return assets_url('css/').$uri;
    }
}

if ( ! function_exists('js_path'))
{
    function js_path($uri = '')
    {
        return assets_url('js/').$uri;
    }
}

if ( ! function_exists('load_css'))
{
    function load_css($paths = array())
    {
        foreach ($paths as $path) {
            echo link_tag(css_path($path));
        }
    }
}

if ( ! function_exists('load_js'))
{
    function load_js($paths = array())
    {
        foreach ($paths as $path) {
            echo '<script type="text/javascript" src="'.js_path($path).'"></script>'."\n";
        }
    }
}

if ( ! function_exists('show_callout'))
{
    function show_callout($message, $type = 'alert')
    {
        if ($message) {
            $content = '';
            if (is_array($message)) {
                $message = array_filter($message);
                foreach ($message as $row) {
                    $content .= "<p>$row</p>\n";
                }
            } else {
                $content = "<p>$message</p>\n";
            }
            echo "<div class='$type callout' data-closable>
            $content
            <button class='close-button' aria-label='Dismiss alert' type='button' data-close>
            <span aria-hidden='true'>&times;</span></button>
            </div>";
        }
    }
}