$(document).foundation();

var nowTemp = new Date();
nowTemp.setDate(nowTemp.getDate() + 1);
var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
var datePicker = $("#dp").fdatepicker({
	format: 'mm-dd-yyyy',
	onRender: function (date) {
		return date.valueOf() < now.valueOf() ? 'disabled' : '';
	}
});
